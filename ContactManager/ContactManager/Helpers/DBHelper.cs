﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using System.Data.SqlClient;
using ContactManager.Models;

namespace ContactManager.Helpers
{
    public static class DBHelper
    {
        public static string connString = "Data Source=.\\SQLEXPRESS;AttachDbFilename=|DataDirectory|Database1.mdf;Integrated Security=True;User Instance=True";
        public static ContactModel ConvertReaderToContactModel(SqlDataReader reader)
        {
            ContactModel temp = new ContactModel();
            temp.FirstName = reader["FirstName"].ToString();
            temp.LastName = reader["LastName"].ToString();
            temp.EmailAddress = reader["EmailAddress"].ToString();
            temp.Address = reader["Address"].ToString();
            temp.Lattitude = (double)reader["Lattitude"];
            temp.Longitude = (double)reader["Longitude"];
            temp.PhoneNumber = reader["PhoneNumber"].ToString();
            temp.id = (int)reader["id"];
            return temp;
        }
    }
}
