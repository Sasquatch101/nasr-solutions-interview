﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using System.Data.SqlClient;
using ContactManager.Helpers;
using System.Data;

namespace ContactManager.Models
{
    public class ContactListModel
    {
        private List<ContactModel> m_contacts;
        public List<ContactModel> Contacts 
        {
            get
            {
                return m_contacts;
            }
            private set
            {
                m_contacts = value;
            }
        }

        public ContactListModel()
        {
            SqlConnection connection = new SqlConnection(DBHelper.connString);

            List<ContactModel> tempList = new List<ContactModel>();
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("SELECT * FROM contacts;", connection);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    tempList.Add(DBHelper.ConvertReaderToContactModel(reader));
                    Console.WriteLine(reader);
                }
                m_contacts = tempList;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void FilterContacts(double lat, double lng)
        {
            //Lat/lng distance calculation
            int closestIndex = 0;
            double currentDistance = double.MaxValue;
            double R = 6371;
            for(int i = 0; i < this.Contacts.Count; i++)
            {
                ContactModel contact = this.Contacts[i];
                double dlon = lng - contact.Longitude;
                double dlat = lat - contact.Lattitude;
                double a = Math.Pow((Math.Sin(dlat/2)),2) + Math.Cos(contact.Lattitude) * Math.Cos(lat) * Math.Pow((Math.Sin(dlon/2)),2);
                double c = 2 * Math.Atan2( Math.Sqrt(a), Math.Sqrt(1-a) );
                double d = R * c;
                if (d < currentDistance)
                {
                    currentDistance = d;
                    closestIndex = i;
                }
            }
            ContactModel temp = this.Contacts[closestIndex];
            this.Contacts.Clear();
            this.Contacts.Add(temp);
        }
        
        //Search for contacts that match search text and type
        public void FilterContacts(string filterText, string filterType)
        {
            using (SqlConnection connection = new SqlConnection(DBHelper.connString))
            {
                List<ContactModel> tempList = new List<ContactModel>();
                try
                {
                    connection.Open();
                    SqlParameter filterTypeParam = new SqlParameter("@filterType", SqlDbType.VarChar);
                    filterTypeParam.Value = filterType;
                    SqlParameter filterTextParam = new SqlParameter("@filterText", SqlDbType.VarChar);
                    filterTextParam.Value = filterText;
                    SqlCommand command = new SqlCommand("SELECT * FROM contacts WHERE " + filterType + " LIKE '%" + filterText + "%';", connection);
                    command.Parameters.Add(filterTypeParam);
                    command.Parameters.Add(filterTextParam);
                    string query = command.CommandText;

                    foreach (SqlParameter p in command.Parameters)
                    {
                        query = query.Replace(p.ParameterName, p.Value.ToString());
                    }
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        tempList.Add(DBHelper.ConvertReaderToContactModel(reader));
                        Console.WriteLine(reader);
                    }
                    m_contacts = tempList;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
        }
    }

    public class ContactModel
    {
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Address")]
        public string Address { get; set; }

        public double Lattitude { get; set; }
        public double Longitude { get; set; }
        public int id { get; set; }

        public ContactModel()
        {

        }

        public ContactModel(int id)
        {
            SqlConnection connection = new SqlConnection(DBHelper.connString);

            try
            {
                connection.Open();
                SqlParameter param = new SqlParameter("@Param1", SqlDbType.Int);
                param.Value = id;
                SqlCommand command = new SqlCommand("SELECT * FROM contacts WHERE id = @Param1;", connection);
                command.Parameters.Add(param);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ContactModel temp = DBHelper.ConvertReaderToContactModel(reader);
                    this.FirstName = temp.FirstName;
                    this.LastName = temp.LastName;
                    this.EmailAddress = temp.EmailAddress;
                    this.Address = temp.Address;
                    this.Lattitude = temp.Lattitude;
                    this.Longitude = temp.Longitude;
                    this.PhoneNumber = temp.PhoneNumber;
                    this.id = temp.id;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
