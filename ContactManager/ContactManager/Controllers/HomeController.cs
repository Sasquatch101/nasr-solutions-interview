﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ContactManager.Models;
using System.Data.SqlClient;
using System.Data;
using ContactManager.Helpers;
using System.Net;
using System.IO;

namespace ContactManager.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Your Contacts";

            
            return View(new ContactListModel());
        }

        public ActionResult Add(ContactModel model)
        {
            if (HttpContext.Request.HttpMethod == "POST")
            {
                SqlConnection connection = new SqlConnection(DBHelper.connString);

                try
                {
                    connection.Open();
                    SqlParameter param1 = new SqlParameter("@firstName", model.FirstName);
                    SqlParameter param2 = new SqlParameter("@lastName", DbType.String);
                    param2.Value = (object)model.LastName ?? DBNull.Value;
                    SqlParameter param3 = new SqlParameter("@emailAddress", model.EmailAddress);
                    param3.Value = (object)model.EmailAddress ?? DBNull.Value;
                    SqlParameter param4 = new SqlParameter("@address", model.Address);
                    param4.Value = (object)model.Address ?? DBNull.Value;
                    SqlParameter param7 = new SqlParameter("@phoneNumber", model.PhoneNumber);
                    param7.Value = (object)model.PhoneNumber?? DBNull.Value;
                    SqlCommand command = null;
                    if (model.Address != null)
                    {
                        //Retrieve lat/lng from Google API using address passed in
                        WebClient client = new WebClient();
                        string html = client.DownloadString("https://maps.googleapis.com/maps/api/geocode/json?address=" + HttpUtility.UrlEncode(model.Address) + "&key=AIzaSyC0jjOVDwxiGm8m07NTbIzsjHJ_aQJ455E");
                        dynamic data = System.Web.Helpers.Json.Decode(html);
                        var latlng = data.results[0].geometry.location;
                        double lat = (double)latlng.lat;
                        double lng = (double)latlng.lng;
                        SqlParameter param5 = new SqlParameter("@lattitude", lat);
                        SqlParameter param6 = new SqlParameter("@longitude", lng);

                        command = new SqlCommand("INSERT INTO contacts " +
                        "(firstName, lastName, emailAddress, address, lattitude, longitude, phoneNumber) " +
                        "VALUES (@firstName, @lastName, @emailAddress, @address, @lattitude, @longitude, @phoneNumber);", connection);
                        command.Parameters.Add(param1);
                        command.Parameters.Add(param2);
                        command.Parameters.Add(param3);
                        command.Parameters.Add(param4);
                        command.Parameters.Add(param5);
                        command.Parameters.Add(param6);
                        command.Parameters.Add(param7);
                    }
                    else
                    {
                        command = new SqlCommand("INSERT INTO contacts " +
                        "(firstName, lastName, emailAddress, address, lattitude, longitude, phoneNumber) " +
                        "VALUES (@firstName, @lastName, @emailAddress, @address, null, null, @phoneNumber);", connection);
                        command.Parameters.Add(param1);
                        command.Parameters.Add(param2);
                        command.Parameters.Add(param3);
                        command.Parameters.Add(param4);
                        command.Parameters.Add(param7);
                    }

                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        ContactModel temp = DBHelper.ConvertReaderToContactModel(reader);
                        return RedirectToAction("View", new { model = temp });
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult Edit(ContactModel model)
        {
            SqlConnection connection = new SqlConnection(DBHelper.connString);

            try
            {
                connection.Open();
                SqlParameter param1 = new SqlParameter("@firstName", model.FirstName);
                SqlParameter param2 = new SqlParameter("@lastName", DbType.String);
                param2.Value = (object)model.LastName ?? DBNull.Value;
                SqlParameter param3 = new SqlParameter("@emailAddress", model.EmailAddress);
                param3.Value = (object)model.EmailAddress ?? DBNull.Value;
                SqlParameter param4 = new SqlParameter("@address", model.Address);
                param4.Value = (object)model.Address ?? DBNull.Value;
                SqlParameter param7 = new SqlParameter("@phoneNumber", model.PhoneNumber);
                param7.Value = (object)model.PhoneNumber ?? DBNull.Value;
                SqlParameter param8 = new SqlParameter("@id", model.id);

                SqlCommand command = null;
                if (model.Address != null)
                {
                    //Get lat/lng from Google API
                    WebClient client = new WebClient();
                    string html = client.DownloadString("https://maps.googleapis.com/maps/api/geocode/json?address=" + HttpUtility.UrlEncode(model.Address) + "&key=AIzaSyC0jjOVDwxiGm8m07NTbIzsjHJ_aQJ455E");
                    dynamic data = System.Web.Helpers.Json.Decode(html);
                    var latlng = data.results[0].geometry.location;
                    double lat = (double)latlng.lat;
                    double lng = (double)latlng.lng;
                    SqlParameter param5 = new SqlParameter("@lattitude", lat);
                    SqlParameter param6 = new SqlParameter("@longitude", lng);

                    command = new SqlCommand("UPDATE contacts " +
                    "SET firstName=@firstName, lastName=@lastName, emailAddress=@emailAddress, address=@address, " +
                    "lattitude=@lattitude, longitude=@longitude, phoneNumber=@phoneNumber " +
                    "WHERE id=@id;", connection);
                    command.Parameters.Add(param1);
                    command.Parameters.Add(param2);
                    command.Parameters.Add(param3);
                    command.Parameters.Add(param4);
                    command.Parameters.Add(param5);
                    command.Parameters.Add(param6);
                    command.Parameters.Add(param7);
                    command.Parameters.Add(param8);
                }
                else
                {
                    command = new SqlCommand("UPDATE contacts " +
                    "SET firstName=@firstName, lastName=@lastName, emailAddress=@emailAddress, address=@address, phoneNumber=@phoneNumber " +
                    "WHERE id=@id;", connection);
                    command.Parameters.Add(param1);
                    command.Parameters.Add(param2);
                    command.Parameters.Add(param3);
                    command.Parameters.Add(param4);
                    command.Parameters.Add(param7);
                    command.Parameters.Add(param8);
                }

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ContactModel temp = DBHelper.ConvertReaderToContactModel(reader);
                    return RedirectToAction("View", new { model = temp });
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            return View(new ContactModel(id));
        }

        public ActionResult View(int id)
        {
            return View(new ContactModel(id));
        }

        public ActionResult Delete(int id)
        {
            SqlConnection connection = new SqlConnection(DBHelper.connString);

            try
            {
                connection.Open();
                SqlParameter param1 = new SqlParameter("@id", id);

                SqlCommand command = new SqlCommand("DELETE FROM contacts " +
                    "WHERE id=@id;", connection);
                command.Parameters.Add(param1);
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return PartialView("ContactPartial", new ContactListModel());
        }

        public ActionResult Search(string SearchText, string Lat, string Long, string Category)
        {
            ContactListModel model = new ContactListModel();
            if (Lat != null && Long != null)
            {
                double lat = double.Parse(Lat);
                double lng = double.Parse(Long);
                model.FilterContacts(lat, lng);
                return PartialView("ContactPartial", model);
            }
            else
            {
                model.FilterContacts(SearchText, Category);
                return PartialView("ContactPartial", model);
            }
        }
    }
}
